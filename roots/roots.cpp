#include "roots.hpp"
#include <cstdio>
#include <cmath>
#include <iostream>
#include "../derivative/derivative.hpp"

using namespace std;

namespace Roots{
	double bissection(double a, double b, size_t max_iter, std::function<double(double)> f, double epsilon){
		//TODO: signal that the method was unable to find a root
		double m = (a+b)/2;
		double delta = epsilon/2;
		size_t iter = 0;
	
		while(iter < max_iter && std::abs(a-b) >= delta){
			if(std::abs(f(m)) < epsilon) break;
			else if(f(a)*f(m) < 0) b = m; //function passes by x-axis in [a;m]
			else a = m; //function passes by x-axis in [m;b]

			m = (a+b)/2;
			iter++;
		}
	
		return m;
	}

	double newton_raphson(double x0, size_t max_iter, std::function<double(double)> f, double epsilon){
		double h = sqrt(epsilon); //to be used by the derivative functions
		double xk=x0, xk1;
		double delta = epsilon/2;
		size_t iter = 0;
		
		while(iter < max_iter){
			iter++;
			double fd = central_derivative(xk,h,f); //derivative

			if(std::abs(fd) < h)
				return xk; //derivative tends to infinity
		
			xk1 = xk - (f(xk)/fd);
		
			if(std::abs(f(xk1)) < epsilon) //good approximation
				return xk1;
			else if(std::abs(xk1-xk) < delta) //between values
				return ((xk1+xk)/2.0);
		
			xk = xk1; 
		}

		return xk;
	}

}

// double f(double x){
// 	return x*x - 2.0;
// 	//return cos(3*x) + 1/3*sin(x);
// }


// int main(){
// 	using namespace std;
// 	double x0 = 1.8;
// 	double nr_root = Roots::newton_raphson(x0, 100000, f);
// 	double bi_root = Roots::bissection(x0-1.0, x0+1.0, 100000, f);

// 	cout << "NR:  " << nr_root << endl;
// 	cout << "\tf(x) = " << f(nr_root) << endl;

// 	cout << "BI:  " << bi_root << endl;
// 	cout << "\tf(x) = " << f(bi_root) << endl;

// 	cout << (double)bi_root << endl;
// 	return 0;
// }
