#ifndef __H_ROOTS_H__
#define __H_ROOTS_H__

#include <functional>
#include "../common/defs.hpp"

namespace Roots{
	double bissection(double a, double b, size_t max_iter, std::function<double(double)> f, double epsilon=EPSILON);
	double newton_raphson(double x0, size_t max_iter, std::function<double(double)> f, double epsilon=EPSILON);
}

#endif //__H_ROOTS_H__
