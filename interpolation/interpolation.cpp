#include "interpolation.h"
#include <string>
#include <cstdlib>
#include <vector>

using namespace std;
using namespace Interpolation;

void usage(char** argv);

int main(int argc, char** argv){
	int num_points = 100; //default number of points to interpolate
	int method = METHOD_LAGRANGE; //default method for interpolation
	if(argc > 2){
		for(int i=1;i<argc;i++){
			string par(argv[i]);
			if(par == "-n"){
				num_points = atoi(argv[++i]);
			}else if(par == "-i"){
				if(argv[i+1][0] == 'l')
					method = METHOD_LAGRANGE;
				else if(argv[i+1][0] == 'n')
					method = METHOD_NEVILLE;
				else{
					cout << "Unkown method " << argv[i+1] << endl;
					usage(argv);
					exit(-1);
				}
				i++;
			}else{
				cout << "Parameter " << par << " unkown." << endl;
				usage(argv);
				exit(-1);
			}
		}
	}

	vector<Point2D> points = read_input_stdin();
	vector<Point2D> intp = interpolate(points, num_points, method);

	for(size_t i=0;i<intp.size();i++){
		cout << intp[i].x << " " << intp[i].y << endl;
	}

	return 0;
}


void usage(char** argv){
	cout << "Usage: " <<  argv[0] << " [-n number of points] [-i interpolation method]" << endl << "\t -i: l for lagrange, n for neville" << endl;
}
