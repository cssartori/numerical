#ifndef __HINTERPOLATION_H__
#define __HINTERPOLATION_H__

#include <vector>
#include <iostream>

namespace Interpolation{
	typedef long double Real; //data type definition
	static const Real EPS = 1e-8; //epsilon for Real comparison
	// The methods available for interpolation
	static const int METHOD_LAGRANGE = 1;
	static const int METHOD_NEVILLE = 2;

	//The 2D point structure
	struct Point2D{
		Real x,y;
	};

	/**
	 * Implementation of Lagrange's interpolation method.
	 **/
	Real lagrange(const Real& xint, const std::vector<Point2D>& p){
		Real lx = 0;
		for(size_t i=0;i<p.size();i++){
			Real li = 1;
			for(size_t j=0;j<p.size();j++){
				if(i == j) continue;
				li = li * (xint-p[j].x)/(p[i].x-p[j].x);
			}
			li = li * p[i].y;
			lx += li;
		}
		return lx;
	}
	
	/**
	 * Implementation of Neville's interpolation method.
	 **/
	Real neville(const Real& xint, const std::vector<Point2D>& p){
		size_t sz = p.size();
		Real nv[sz][sz]; //neville's matrix

		//initialization of main diagonal
		for(size_t i=0;i<sz;i++)
			nv[i][i] = p[i].y;

		size_t i=0, j=1, col=1;
		while(j < sz){
			Real z1 = (p[j].x - xint)*nv[i][j-1]; //first part
			Real z2 = (xint - p[i].x)*nv[i+1][j]; //second part
			nv[i][j] = (z1+z2)/(p[j].x-p[i].x);
			j++;
			i++;
			if(j == sz){ //end of diagonal: reset j and i
				i=0;
				j = ++col;
			}
		}

		//final result is in position nv[0][k]
		return nv[0][sz-1];
	}

	/**
	 * Compute the interpolated y value.
	 **/
	Real compute_yint(const Real& xint, const std::vector<Point2D>& p, const int& method){
		Real yint = 0;
		switch(method){
		case METHOD_LAGRANGE:
		    yint = lagrange(xint, p);
			break;
		case METHOD_NEVILLE:
			yint =  neville(xint, p);
			break;
		}
		return yint;
	}
	
	/**
	 * Merge two vector of points keeping them ordered according to the x value.
	 **/
	std::vector<Point2D> merge_points(const std::vector<Point2D>& p, const std::vector<Point2D>& q){
		std::vector<Point2D> merged;
		size_t i=0,j=0;
		while(i < p.size() && j < q.size()){
			Real diff =  q[j].x - p[i].x;
			if(diff > EPS){
				merged.push_back(p[i]);
				i++;
			}else{
				merged.push_back(q[j]);
				j++;
			}
		}
		
		while(i < p.size()) merged.push_back(p[i++]);
		while(j < q.size()) merged.push_back(q[j++]);

		return merged;
	}
	
	/**
	 * Interpolates the k given points in vector 'points', creating 'num_points' new data points.
	 * The new points are set between xmin and xmax from 'points'. The interpolation method is given
	 * in 'method' (default is Lagrange).
	 **/
	std::vector<Point2D> interpolate(const std::vector<Point2D>& points, const size_t& num_points, const int& method = METHOD_LAGRANGE){
		Real xmin = points.front().x;
		Real xmax = points.back().x;
		Real step = (xmax-xmin)/num_points;
		
		std::vector<Point2D> intp; //interpolated points
		Real x = xmin+step; //current x to interpolate
		while(xmax - x > EPS){
			Point2D pt = {x,0}; //new interpolated point
			pt.y = compute_yint(pt.x, points, method);
			intp.push_back(pt);
			x += step;
		}
		
		return merge_points(points, intp);
	}

	/**
	 * Read input from stdin. Points should be formated as:
	 * x0 y0
	 * x1 y1
	 * x2 y2
	 * ...
	 **/
	std::vector<Point2D> read_input_stdin(){
		Point2D p;
		std::vector<Point2D> points;
	
		while(std::cin >> p.x >> p.y){
			points.push_back(p);
		}
	
		return points;
	}

	
};

#endif //__HINTERPOLATION_H__
