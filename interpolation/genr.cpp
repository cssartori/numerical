#include <cstdlib>
#include <random>
#include <cmath>
#include <iostream>

using namespace std;

typedef long double Real;
static const int R = 1;
int FUNC = 1;
Real P = 0;
int N = 100;
Real X0 = -N/2;

std::default_random_engine generator(1);
std::uniform_real_distribution<Real> rand_p(0,1);
	
Real f1(Real x, Real p=0){
	Real ff = x*x;
	ff += rand_p(generator) < p ? rand_p(generator) : 0;
	return ff;
}

Real f2(Real x, Real p=0){
	Real ff = x*x*x;
	ff += rand_p(generator) < p ? rand_p(generator) : 0;
	return ff;
}

Real f3(Real x, Real p=0){
	Real ff = sin(x);
	ff += rand_p(generator) < p ? rand_p(generator) : 0;
	return ff;
}

Real f4(Real x, Real p=0){
	Real ff = log(x);
	ff += rand_p(generator) < p ? rand_p(generator) : 0;
	return ff;
}

Real y(Real x, int f){
	switch(f){
	case 1:
		return f1(x, P);
	case 2:
		return f2(x, P);
	case 3:
		return f3(x, P);
	case 4:
		return f4(x, P);
	default:
		cout << "Function " << f << " unkown." << endl;
		exit(-1);
	}
	return 0;
}

int main(int argc, char** argv){
	if(argc > 1){
		FUNC = atoi(argv[1]);
	}
	if(argc > 2){
		P = atof(argv[2]);
	}
	if(argc > 3){
		N = atoi(argv[3]);
	}
	
	Real x = X0;
	for(int i=0;i<N;i++){
		cout << x << " " << y(x, FUNC) << endl;
		x += 1;
	}
}
