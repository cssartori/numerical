/**
 * Implements the numerical derivatives of a given function f.
 * Two methods are implemented: 
 *             1) by forward difference method,
 *             2) by central difference method.
 * It uses library <functiontal> from C++11
 * 
 * Carlo S. Sartori - 2013/2
 */

#ifndef __H_DERIVATIVE_H__
#define __H_DERIVATIVE_H__

#include <cmath>
#include <functional>

double forward_derivative(double x, double h, std::function<double(double)> f){
	double fd = f(x+h)-f(x); //forward difference
	return fd/h;
}

double central_derivative(double x, double h, std::function<double(double)> f){
	double cd = f(x+(0.5*h))-f(x-(0.5*h)); //central difference
	return cd/h;
}



#endif //__H_DERIVATIVE_H__
