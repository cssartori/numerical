/****************************************************************************
*****************************************************************************
	Author: Carlo Sulzbach Sartori
	Course: Numerical Methods	-	UFRGS 2013/2

	./sqrt -n N1 -e N2
	-n is the number to calculate the square root from
	-e is the error precision to accept a given root
	
*****************************************************************************
****************************************************************************/

#include <stdio.h>
#include <stdlib.h>

/*Absolute function (not using math.h)*/
double abs2(double n) {
	if(n < 0)
		n = n*-1;

	return n;
}

/*Receives a number n and a precision e. Returns the square root of n up to a given precision based on e.*/
double sqrt2(double n, double e) {
	double x = n/2;
	double xn1;
	double xt = x;

	do {
		x = xt;
		double temp = x+(n/x);
		xn1 = .5*temp;
		xt = xn1;
	} while((abs2(xn1 - x)) >= e );

	return xn1;
}

/* Print command line help arguments */
void usage_error(char **argv) {
	printf("Usage:\n\t%s -n N1 -e N2\n"
			"n: the value to calculate the squarte root from (sqrt(n))\n"
			"e: the error precision to accept a given root\n",
			argv[0]);
}

/* Reads the paramenters given in the command line */
void parameter_reading(int argc, char **argv, double *n, double *e){
	if(argc < 5) {
		usage_error(argv);
		exit(-1);
	}
	else {
		int i=1;
		for(;i<5;i++) {
			if(argv[i][0] == '-') {
				switch(argv[i][1]) {
				case 'n':
					i++;
					*n = atof(argv[i]);
				break;
				case 'e':
					i++;
					*e = atof(argv[i]);
				break;
				default:
					printf("Unknown parameter %s\n", argv[i]);
					usage_error(argv);
					exit(-1);
				}
			}
			else {
				printf("Unknown parameter %s\n", argv[i]);
				usage_error(argv);
				exit(-1);
			}
		}
	}
}


int main(int argc, char **argv) {
	double n, e;
	
	parameter_reading(argc, argv, &n, &e);
	
	if(n < 0){
		printf("***Input value n is negative. Can\'t compute square root.\n");
		return -1;
	}
	
	printf("sqrt(%lf) = %lf\n", n, sqrt2(n, e));
}
