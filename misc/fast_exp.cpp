#include <iostream>

using namespace std;

typedef long double Real;

//computes x^n, given n integer
Real fast_exp(Real x, int n){
	if(n == 0) return 1;
	if(n == 1) return x;

	if(n % 2 == 0){
		return fast_exp(x*x, n/2);
	}else{
		return x*fast_exp(x*x, (n-1)/2);
	}
}

Real naive_exp(Real x, int n){
	if(n==0) return 1;
	return x*naive_exp(x,n-1);
}


int main(){
	Real x = 2;
	int n = 30;
	Real fe = fast_exp(x,n);
	Real ne = naive_exp(x,n);
	
	cout << "fe(" << x << "," << n << ") = " << fe << endl;
	cout << "ne(" << x << "," << n << ") = " << ne << endl;
}
