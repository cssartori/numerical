import numpy as np;


def gauss_jordan(A,b):
    '''
    Matrix A is the coefficient matrix, and b is the vector of independent
    elements. Given a solution vector x, they are so that the method solves:
        Ax = b
    It uses partial pivoting only to remove null pivots.
    
    Return: vector x with solution
    '''    
    #concatenate matrix A with vector b -> [A:b]
    Y = np.zeros((A[0].size, A[:,0].size+1))
    Y[:,0:A[0].size] = A[:,:]
    Y[:,A[:,0].size] = b
    
    Y = __pivoting__(Y)    
    x = __backsubstitution__(Y)    

    return x

def __pivoting__(Y):
    '''
    Given an extended matrix Y = [A:b], execute pivoting to make it triangular.
    The pivoting is partial, in the way it only look for rows below without 
    null pivots, but not for columns.
    
    Return: pivoted matrix Y.
    '''
    for j in range(0,Y[0].size-1):
        if Y[j,j] == 0:
            Y = __partial_permutation__(Y,j)
            
        for i in range(j+1,Y[:,0].size):
            Y[i] = Y[i] - (Y[i,j]/Y[j,j])*Y[j]
        
    return Y

def __backsubstitution__(Y):
    '''
    Given a pivoted extended matrix Y, solves the linear system by 
    backsubstituiton method.
    
    Return: vector x with solution.
    '''
    x = np.zeros(Y[:,0].size)
    
    for i in range(Y[:,0].size-1,-1,-1):
        sum = 0
        for j in range(i+1,Y[0].size-1):
            sum += Y[i,j]*x[j]
        sum = Y[i,-1] - sum
        x[i] = sum/Y[i,i]
    
    return x

def __partial_permutation__(Y, i):
    '''
    Given an extended matrix Y and a row i of Y with pivot zero (Y[i,i] == 0), 
    try to find a following row (i+1,i+2,...) that has pivot not null.
    Partial pivoting method.

    Return: matrix Y permuted, or error.
    '''
    for k in range(i+1,Y[:,0].size):
        if Y[k,i] != 0:
            r = np.array(Y[k,:])
            Y[k,:] = np.array(Y[i,:])
            Y[i] = r
            break
    
    return Y
    


#For testing purposes:
#    
#A = np.zeros((4,4))
#b = np.zeros(4)
#
#A[:,:] = [[0,2,0,1],
#          [9,8,-3,4],
#          [-6,4,-8,0],
#          [3,-8,3,-4]]
#
#b[:] = [3,6,-16,18]
#
#x = gauss_jordan(A,b)
#        
#print x     
